"""Utils."""

import xml.etree.ElementTree as ET


def generate_empty_element(name: str) -> ET.Element:
    """Generate an empty ElementTree (Tag) given a name."""
    return ET.fromstring(f'<{name}/>')
