"""Render Templates."""

import argparse
from functools import reduce
from importlib.resources import files
from operator import concat
import sys
import typing

import jinja2

ARCHITECTURES: typing.List[str] = ['aarch64', 's390x', 'ppc64le', 'x86_64']
DISTROS: typing.List[str] = [
    'CentOSStream9',
    'Fedora35',
    'Fedora36',
    'Fedora37',
    'FedoraELN',
    'Fedorarawhide',
    'RedHatEnterpriseLinux7',
    'RedHatEnterpriseLinux8',
    'RedHatEnterpriseLinux9',
]
TEMPLATES_DIR = str(files(__package__).joinpath('templates'))
SMOKE_TESTS: typing.List[str] = ['abort_recipe', 'abort_task', 'fail',
                                 'kernel_panic', 'skip', 'pass', 'warn']


def get_template_variables(
    parsed_vars: typing.Dict[str, typing.Any],
) -> typing.Dict[str, typing.Any]:
    """Clean and return variables to generate templates."""
    if not parsed_vars['smoke_test']:
        smoke_test = SMOKE_TESTS
    else:
        smoke_test = [*set(reduce(concat, parsed_vars['smoke_test']))]  # type: ignore

    return {
        'arch': parsed_vars['arch'],
        'distro': parsed_vars['distro'],
        'smoke_test': smoke_test
    }


def render(template_file: str, data: typing.Dict[str, typing.Any]) -> str:
    """Render Beaker Template."""
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(TEMPLATES_DIR),
        trim_blocks=True,
        keep_trailing_newline=True,
        lstrip_blocks=True,
        autoescape=jinja2.select_autoescape(
            enabled_extensions=('xml'),
            default_for_string=True,
        ),
        undefined=jinja2.StrictUndefined,
    )

    return env.get_template(template_file).render(data)


def main(args: typing.List[str]) -> None:
    """Run the main CLI interface."""
    parser = argparse.ArgumentParser(
        description='Render Beaker XML for upt smoke tests',
    )

    parser.add_argument('--smoke-test', nargs='+', action='append', choices=SMOKE_TESTS,
                        help=f'Select smoke test. Defaults all smoke tests {SMOKE_TESTS}')

    parser.add_argument('--arch', type=str, default='x86_64', choices=ARCHITECTURES,
                        help='Select the architecture. Defaults to x86_64')

    parser.add_argument('--distro', type=str, default='CentOSStream9', choices=DISTROS,
                        help='Select the distro. Defaults to CentOSStream9')

    parser.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout,
                        help='Output path for the rendered file. Defaults STDOUT')

    parsed_args = parser.parse_args(args)

    template_file = 'smoke_test.j2'
    template_variables = get_template_variables(vars(parsed_args))

    result = render(template_file, template_variables)
    parsed_args.output.write(result)


if __name__ == '__main__':
    main(sys.argv[1:])
